import React, { useEffect, useState } from 'react';
import { View, Text, Button, TextInput, StyleSheet, ImageBackground, SafeAreaView, FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();


function Home({ navigation }) {
  return (
    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Welcome</Text>
      <ImageBackground
        source={{
          uri: 'https://images.unsplash.com/photo-1617634667039-8e4cb277ab46?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxleHBsb3JlLWZlZWR8MTB8fHxlbnwwfHx8fHw%3D',
        }}
        resizeMode="stretch"
        style={{ width: 500, height: 800, position: "absolute" }}
      ></ImageBackground>
      <Button title="click here " color="orange" onPress={() => navigation.navigate('Details')} />
    </View>
  );
}

function DetailsScreen({ navigation }) {
  const [Emailid, setEmailid] = React.useState('');
  const [password, setPassword] = React.useState('');

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Fill up data</Text>
      <TextInput
        style={styles.input}
        value={Emailid}
        placeholder={"Emailid"}
        onChangeText={(text) => setEmailid(text)}
        autoCapitalize={"none"}
      />
      <TextInput
        style={styles.input}
        value={password}
        placeholder={"Password"}
        secureTextEntry
        onChangeText={(text) => setPassword(text)}
      />
      <Button title="Login" onPress={() => navigation.navigate('MainScreen', { name: 'CityName' })} />
    </View>
  );
}

function MainScreen({ route }) {
  const { name } = route.params;
  const [data, setData] = useState([]);

  useEffect(() => {
    
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${name}&appid=YOUR_API_KEY`)
      .then(res => res.json())
      .then(res => setData(res))
      .catch(err => console.log(err));
  }, [name]);

  return (
  
     
      <FlatList
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View>
            <Text>{item.name}</Text>
          </View>
        )}
      />
    
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Details" component={DetailsScreen} />
        <Stack.Screen name="MainScreen" component={MainScreen} />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            title: 'Welcome to my App',
            headerTitleAlign: 'center',
            headerStyle: {
              backgroundColor: 'orange',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    flexWrap: "wrap",
    borderColor: 'black',
    borderWidth: 1,
    marginBottom: 10,
    padding: 10,
    width: 200,
  },
});

export default App;
