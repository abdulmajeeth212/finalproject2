import * as React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';  // Import TextInput and StyleSheet

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
function signup({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="welcome to Signup page"
          onPress={() => navigation.navigate('home')}
        />
      </View>
    );
  }
  const usernameValue = username;
  const passwordValue = password;
  // Since the signUp method returns a Promise, we need to call it using await
  return await Parse.User.signUp(usernameValue, passwordValue)
    .then((createdUser) => {
      // Parse.User.signUp returns the already created ParseUser object if successful
      Alert.alert(
        'Success!',
        `User ${createdUser.getUsername()} was successfully created!`,
      );
      return true;
    })
    .catch((error) => {
      // signUp can fail if any parameter is blank or failed an uniqueness check on the server
      Alert.alert('Error!', error.message);
      return false;
    });
   

  function App() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Details">
          <Stack.Screen name="signup" component={signUp} />
           <Stack.Screen name="details" component={DetailsScreen} />
         </Stack.Navigator>
      </NavigationContainer>
    );
  }